var playpause = document.getElementById("play");
const trackTitles = document.getElementsByClassName("track-title")



document.addEventListener("deviceready", ()=> {

  const music = new Media("https://music.dunarr.app/track/2720")
  let playing = false;

  playpause.addEventListener("click", ()=>{
      if (!playing) {
        playpause.title = "Pause";
        music.play();
        playing = true
      } else {
        playpause.title = "Play";
        music.pause();
        playing = false
      }
  })



  const playlistElement = document.getElementById("playlist")
  axios.get("https://music.dunarr.app/folders/197").then(displayTracks)
const searchBar = document.getElementById("search-bar")
  searchBar.addEventListener("keypress", ()=> {
    console.log(searchBar.value)
    axios.get("https://music.dunarr.app/search?q=" + searchBar.value).then(displayTracks)
  })



  function displayTracks({data}){
    playlistElement.innerHTML = ""
    let counter = 1
    for (const track of data.tracks) {

      const trackHTML = document.createElement("tr")

      trackHTML.addEventListener("click", function (){
        // console.log(`https://music.dunarr.app/track/` + track.id);
        music.stop()
        music.src = `https://music.dunarr.app/track/` + track.id
        music.play()
        playing = true
        playpause.title = "Pause";
        document.getElementById("play").checked = true
        console.log("playing");

        for (const trackTitle of trackTitles) {
          trackTitle.innerText = track.name
        }
      })

      trackHTML.innerHTML=`
         <td class="nr">
                <h5>${counter}</h5></td>
            <td class="title"><h6>${track.name}</h6></td>
            <td class="length"></td>
            <td><input type="checkbox" id="heart"/><label class="zmr" for="heart"></label></td>`

      playlistElement.appendChild(trackHTML)
      counter++
    }
  }
}, false);



